# -*- coding: utf-8 -*-
from __future__ import unicode_literals #Ajout encodage
from __future__ import division #Ajout Division

import sys
reload(sys)
sys.setdefaultencoding('utf8')


from psychopy import core, visual, event, data, gui
import csv, random, time


#### PREPARATION DE L'EXPERIENCE ####




#Chemin des fichiers

stim_path = './stims/'
amorces_path = './amorces/'
data_path = './data/'

#GUI de recueil de données relatives au participant

exp_info = {'sujet':'', 'age':'', 'sexe':['F','H'],'lateralite':['D','G']}
dlg = gui.DlgFromDict(dictionary = exp_info)
if dlg.OK == False:
   core.quit()

#Préparation de l'affichage

win = visual.Window([1440, 900], color = (256,256,256), fullscr = True)

#Récupération des amorces depuis un fichier excel
#Organisation aléatoire de leur présentation sur les 3 blocs tests, 1 seule répétition

amorces_r_0 = data.importConditions(amorces_path + 'Amorces0.xlsx')
amorces_0 = data.TrialHandler(amorces_r_0,1,method = 'random')
amorces_r_1 = data.importConditions(amorces_path + 'Amorces1.xlsx')
amorces_1 = data.TrialHandler(amorces_r_1,1,method = 'random')
amorces_r_2 = data.importConditions(amorces_path + 'Amorces2.xlsx')
amorces_2 = data.TrialHandler(amorces_r_2,1,method = 'random')
amorces_r_3 = data.importConditions(amorces_path + 'Amorces3.xlsx')
amorces_3 = data.TrialHandler(amorces_r_3,1,method = 'random')

ordreAmorce = [0]
randomAmorce = [1,2,3]
random.shuffle(randomAmorce)
ordreAmorce.extend(randomAmorce)

#Affectation aléatoire des formes (C Carré,T Triangle,R Rond) associées à une étiquette (M Moi,A Ami,D Autre) : formation de la liste de nos cibles congruentes

items1 = ["CM","TD","RA"]
items2 = ["CD","TA","RM"]
items3 = ["CA","TM","RD"]
nombreAleatoire=random.randrange(1,3)
affect = eval("items"+str(nombreAleatoire))

#Récupération des données démographiques dans le fichier excel

fileName ="SUJET"+exp_info.get("sujet")
datafile = open(data_path+fileName+".csv", "wb")    #En fonction de la version de Python mettre l'argument "wb" OU "w" et enlever/ajouter newline=""
writer = csv.writer(datafile,delimiter=str(u';').encode('utf-8')) #ajout encodage
#writer = csv.writer(datafile, delimiter=";") Version Windows
writer.writerow([exp_info.get("sujet"),exp_info.get("age"),exp_info.get("sexe"),exp_info.get("lateralite")])
writer.writerow(affect) 	#permet de récupérer les 3 associations formes/labels affectées aléatoirement par participant 
writer.writerow(["bloc","essai","cible", "congruence","repClavier","BR", "TR","liste","amorce","valence","intensiteValence","eveil","emotion"])
 
    #bloc = numéro du bloc, essai = numéro de l’essai, cible = cible présentée sur l’essai (ex : RM, TD, CA …), 
    #congruence = congruence ou incongruence de l’association présentée sur chaque essai, repClavier = réponse clavier du participant, 
    #BR= justesse de la réponse, TR = temps de réaction, liste = numéro de la liste à laquelle appartient l’amorce, valence = valence de l’amorce, 
    #intensiteValence = intensité de la valence de l’amorce, eveil = niveau d’éveil associé à l’amorce, emotion = émotion associée à l’amorce

    #Remarque : la récupération du niveau d’éveil ainsi que de l’émotion associée à l’amorce répond à des besoins extrinsèques à la réalisation de ce travail 
    #mais seront éventuellement exploités pour des analyses postérieures. 


#Préparation de la liste aléatoire de stimuli (120 stimuli)


    #Il s’agit de faire en sorte que la moitié des cibles soient congruentes, l’autre moitié incongruentes 
    #et que chaque forme + étiquette soient équiréparties au sein de chaque bloc (⅓ ; ⅓ ; ⅓)

r_items = ["CM", "CD", "CA", "TM", "TD", "TA", "RM", "RD", "RA"]

    #on ajoute à la liste des 9 combinaisons, les 3 associations présentées à l’encodage de sorte que 
    #la moitié des associations présentées seront congruentes, l’autre moitié incongruentes

r_items.extend(affect) 
random.shuffle(r_items)         #r_items est exploitée pour le bloc d’entraînement
s_items = r_items*10        #120 items par blocs
random.shuffle(s_items)     #s_items est exploitée pour les 3 blocs de test


#Gestion de la latéralité :
 
    #la réponse “oui” est présentée sur la main dominante du participant
    #la consigne est donc ajustée en fonction de la latéralisation du participant

lateralite=exp_info.get("lateralite")
if lateralite=='D': #ATTENTION SUBITILITE LINUX (enlever les crochets)
    correct="k"
    incorrect="s"
    Textconsigne="Dans chaque essai, appuyez sur \"k\", lorsque la forme est associée au bon mot. Sinon appuyez sur \"s\"."
elif lateralite=='G':#ATTENTION SUBITILITE LINUX (enlever les crochets)
    correct="s"
    incorrect="k"
    Textconsigne="Dans chaque essai, appuyez sur \"s\", lorsque la forme est associée au bon mot. Sinon appuyez sur \"k\"."
    
#### PASSAGE DE L'EXPERIENCE ####


#Consignes

consigne = visual.TextStim(win,alignHoriz='center',color="black")
consigne.setText("Vous allez voir apparaître à l'écran trois paires composées d'une forme et d'un mot. Vous allez devoir mémoriser chacune de ces paires.")
consigne.draw()
win.flip()
core.wait(10)

#Ajustement de la consigne en fonction des 3 associations extraites aléatoirement et stockées dans affect

txtConsigne = visual.TextStim(win,alignHoriz='center',color="black")
ligne = ""
for a in range(len(affect)):

    if affect[a][0] == 'C' :
        forme = "Carré"
    elif affect[a][0] == 'T' :
        forme = "Triangle"
    elif affect[a][0] == 'R' :
        forme = "Rond"

    if affect[a][1] == 'M' :
        label = "Moi"
    elif affect[a][1] == 'A' :
        label = "Ami"
    elif affect[a][1] == 'D' :
        label = "Autre"

    ligne += forme + " = " + label + "\n" #présentation des associations à encoder

#Affichage

txtConsigne.setText(ligne)
txtConsigne.draw()
win.flip()
core.wait(10)

#Affichage de la consigne réponse clavier paramétrée en amont en fonction de la latéralisation du participant

consigne = visual.TextStim(win,alignHoriz='center',color="black")
consigne.setText(Textconsigne)
consigne.draw()
win.flip()
core.wait(10)



#Paramètres de l'expérience : 1 bloc d'entrainement + 3 blocs de passation

nbbloc = 0      #numéro du bloc de stimuli
numitem = 1     #numéro de l’essai dans le bloc


#Passation des blocs de stimuli

while nbbloc < 4 :
        
    # Paramètrages intra-blocs
    histocond = 0       #compteur de bonnes réponses
    u = 0               #gestion de la boucle des cibles (forme + étiquette)
    liste_amorce = ordreAmorce[nbbloc]
    liste = eval("amorces_"+str(liste_amorce)) #implémentation de la liste des amorces (pour rappel, l’ordre de déroulement des listes est randomisée entre les participants)
    #Remarque liée à la propreté du code : nous ne pouvions pas faire tourner cette boucle à l’aide de la variable i utilisée pour dérouler la liste des amorces 
    #car celle-ci fonctionne avec la fonction PsychoPy trialHandler (documentation : http://www.psychopy.org/api/data.html). 
    #La variable i correspond donc à une variable dictionnaire qui peut UNIQUEMENT être utilisée pour gérer les fichiers Excel qui stockent nos amorces, 
    #d’où la création d’une variable u pour dérouler les cibles.
    
    # Paramétrage du type de bloc (entraînement ou test)

    if nbbloc == 0 :
        bloc = "d'entraînement" 		#pour l'affichage du message de début du bloc
        liste_items = r_items 			#liste des amorces = liste d’entraînement

    else :
        bloc = str(nbbloc) 				#pour l'affichage du message de début du bloc avec affichage du numéro du bloc
        liste_items = s_items 			#liste des amorces = liste test 1, 2 ou 3         

    #Affichage du message de début de bloc

    blocDeb = visual.TextStim(win, color="blue")
    blocDeb.setText("Début du bloc "+bloc)
    blocDeb.draw()
    win.flip()
    core.wait(1.0)
    
    #Début du bloc

    for i in liste:
        
        #Croix de fixation

        image = visual.ImageStim(win, image =stim_path+"croix.jpg")
        image.draw()
        win.flip()
        core.wait(.5)
        
        #Amorce
        
            #Récupération des variables de nos fichiers amorces à stocker dans le fichier de sortie
            #Remarque : la récupération du niveau d’éveil et de l’émotion associée à l’amorce n’est pas directement utile aux analyses du présent travail

        am = i['Amorce']
        val = i['Valence']
        Ival = i['Ival']
        eveil = i['Arousal']
        emotion = i['Emotion']
        
            #Affichage amorce

        amorce = visual.TextStim(win,color="black")
        amorce.setText(am)
        amorce.draw()
        win.flip()
        core.wait(.15)
        
        #Masque (cf. SOA du paradigme d’amorcage affectif) 
        
        image = visual.ImageStim(win, stim_path+"Masque.jpg")
        image.draw()
        win.flip()
        core.wait(0.1)
        
        #Stimulus/cible (forme + étiquette)
        
        cible = visual.ImageStim(win, image = stim_path + liste_items[u] + ".jpeg")
        cible.draw()
        win.flip()
        core.wait(.1)
    
            #Gestion de la variable temps de réaction de notre fichier de sortie
            
        imageTime = time.time()         #variable temps associée à l'image
        key = event.waitKeys(1.000, ['s','k', 'escape'])
        responseTime = time.time()      #variable temps de réponse
        
        #Prise en compte de la congruence des items
        
        if (liste_items[u] in affect) == True:
            cong=1
        else :
            cong=0
        
        #Prise en compte de la réponse du sujet (s = vrai, k = faux si gaucher et inversement si droitier) 
            #cf. paramétrage des valeurs de “correct” et “incorrect” au début de notre code)
        
        if key == None:                                                         #Absence de réponse
            key=[]
            key.append("no key")
            cond = 0
            miss=1                                                              #gestion d’une absence de réponse pour le feedback
        elif key[0] == correct and (liste_items[u] in affect) == True :         #Hit correct
            cond = 1
        elif key[0] == incorrect and (liste_items[u] in affect) == False :      #Rejet correct
            cond = 1
        elif key[0] =="escape":                                                 #possibilité de quitter l’expérience avec la touche echap
            datafile.close()
            win.close()
            core.quit()
        else :                                                                  #Mauvaise réponse
            cond = 0
            miss=0

        histocond += cond       #incrémentation du compteur de bonnes réponses 
    
        #Ecriture du fichier de sortie

            #enregistre le timing de réponse selon une chaîne de 3 caractères
            
        writer.writerow([nbbloc, numitem, liste_items[u], cong, key[0], cond, "{:.3f}".format(responseTime-imageTime),liste_amorce,am,val,Ival,eveil,emotion]) 
        numitem += 1                #incrémentation du numéro de l’essai
        event.clearEvents()         #réinitialiser des événements clavier
        
        #Feedback
        
        if cond == 1 :                 #réponse correcte
            stim = visual.TextStim(win, color="green")
            stim.setText("correct")
            stim.draw()
            win.flip()
            core.wait(0.5)
        elif cond == 0 and miss == 1 :      #réponse manquante
            stim = visual.TextStim(win,color="blue")
            stim.setText("Vous n'êtes pas assez rapide !")
            stim.draw()
            win.flip()
            core.wait(0.5)
        else :                              #réponse incorrecte
            stim = visual.TextStim(win,color="red")
            stim.setText("faux")
            stim.draw()
            win.flip()
            core.wait(0.5)
        
        u += 1   #incrémentation de la variable qui permet de faire tourner la liste des cibles (forme + étiquette)

        
        #Intervalle inter-items (consiste en un tirage aléatoire entre 3 valeurs)
        
        image = visual.ImageStim(win, stim_path + "Masque.jpg")
        image.draw()
        win.flip()
        listinterv = [0.8,1.0,1.2] 
        choix = random.randrange(1,3)
        interv = listinterv[choix]
        core.wait(interv)
        
        
        
        
    #Pause entre chaque bloc de test

    #Affichage du compteur de bonnes réponses
    
    if nbbloc == 0:                 #bloc d’entraînement à 12 essais
        taux = (histocond/12)*100
    else:                           #blocs tests à 120 essais
        taux = (histocond/120)*100
    
    taux="{:.2f}".format(taux)       #présentation du taux arrondi à 2 chiffres après la virgule
    blocScore = visual.TextStim(win,alignHoriz = 'center', color = "black")
    blocScore.setText("Votre taux de bonnes réponses est de " + taux + " %")
    blocScore.draw()
    win.flip()
    core.wait(4.0)
    
    blocFin = visual.TextStim(win, alignHoriz = 'center',color = "black")
    if nbbloc == 4:
        blocFin.setText("Fin du bloc " + bloc)
    else:
        blocFin.setText("Fin du bloc " + bloc + ", pause de 30 secondes")
    blocFin.draw()
    win.flip()
    core.wait(26.0)
    
    numitem = 1         #réinitialisation du numéro de l’essai avant lancement d’un nouveau bloc
    nbbloc += 1         #incrémentation du numéro du bloc



#Message de fin d'expérience

messageFin = visual.TextStim(win, alignHoriz='center', color="black")
messageFin.setText("Merci pour votre participation")
messageFin.draw()
win.flip()
core.wait(5.0)

#Fermeture des fichiers d’entrée et de sortie et de la fenêtre d’affichage

datafile.close()
win.close()
core.quit()
